export class AuthUserModel {
    displayName: string;
    email: string;
    phoneNumber: string;
    photoUrl: string;
}
