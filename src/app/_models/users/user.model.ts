export interface DefaultGroupModel {
    id: string;
    name: string;
}

export class UserModel {
    id?: string;
    firstname: string;
    lastname: string;
    email: string;
    mobileNo: string;
    password?: string;
    roles?: string[];
    defaultGroup: DefaultGroupModel;
    groups?: string[];
}
