import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFoundComponent } from '@shared/components/not-found/not-found.component';
import { AuthenticatedGuard } from '@shared/guards/authenticated.guard';
import { NotAuthenticatedGuard } from '@shared/guards/not-authenticated.guard';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./_modules/home/home.module').then(mod => mod.HomeModule),
    canActivate: [NotAuthenticatedGuard],
  },
  {
    path: 'auth',
    loadChildren: () => import('./_modules/auth/auth.module').then(mod => mod.AuthModule),
    canActivate: [NotAuthenticatedGuard],
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./_modules/dashboard/dashboard.module').then(mod => mod.DashboardModule),
    canActivate: [AuthenticatedGuard],
  },
  {
    path: 'wallet',
    loadChildren: () => import('./_modules/wallet/wallet.module').then(mod => mod.WalletModule),
    canActivate: [AuthenticatedGuard],
  },
  {
    path: 'contribution',
    loadChildren: () => import('./_modules/contribution/contribution.module').then(mod => mod.ContributionModule),
    canActivate: [AuthenticatedGuard],
  },
  {
    path: 'loan',
    loadChildren: () => import('./_modules/loan/loan.module').then(mod => mod.LoanModule),
    canActivate: [AuthenticatedGuard],
  },
  {
    path: '**',
    component: NotFoundComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
