import { Component } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import {
  Event,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router
} from '@angular/router';

import { RootState, setAuthUser } from '@rootstore/index';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SmartLend';

  constructor(
    private router: Router,
    private spinner: NgxSpinnerService,
    private store: Store<RootState>
  ) {

    store.dispatch(setAuthUser());

    spinner.show();
    router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.spinner.show();
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.spinner.hide();
          break;
        }
        default: {
          break;
        }
      }
    });
  }

}
