import { Component, OnInit, Input, ViewChildren, QueryList, ElementRef } from '@angular/core';

@Component({
  selector: 'app-close',
  templateUrl: './close.component.html',
  styleUrls: ['./close.component.scss']
})
export class CloseComponent implements OnInit {
  @ViewChildren('codes') codes: QueryList<ElementRef>;
  @Input() back: string;
  @Input() dark: boolean;

  constructor() { }

  ngOnInit() {
  }

}
