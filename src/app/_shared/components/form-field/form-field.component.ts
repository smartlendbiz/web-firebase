import { Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => FormFieldComponent)
    }
  ]
})
export class FormFieldComponent implements ControlValueAccessor {
  @Input() placeholder = '';
  @Input() label: string;
  @Input() disabled = false;
  @Input() type: string;
  @Input() errorMessage = '';
  @Input() isValid: boolean;
  @Input() class: string;
  @Input() preText: string;
  @Input() maxlength = '';

  value: any = '';
  isShowPassword: boolean;

  propagateChange = (_: any) => { };
  touched = () => { };

  valueChanged(value): void {
    this.value = value;
    this.propagateChange(value);
  }

  onBlur(): void {
    this.touched();
  }

  writeValue(value: any) {
    this.value = value;
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn) {
    this.touched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  clear() {
    this.value = '';
  }

  togglePassword() {
    this.isShowPassword = !this.isShowPassword;
  }
}
