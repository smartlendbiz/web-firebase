import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'inputError'
})
export class InputErrorPipe implements PipeTransform {

    transform(errors: any, args?: any): string {
        if (!args) {
            return null;
        }

        console.log(errors, args)
        if (args.submitted) {
            // tslint:disable-next-line: forin
            for (const error in errors) {
                switch (error) {
                    case 'required':
                        return 'This field is required';
                        break;
                    case 'email':
                        return 'Invalid email format';
                        break;
                }
            }
        }

        return null;
    }

}
