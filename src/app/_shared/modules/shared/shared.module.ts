import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FormFieldComponent } from '@shared/components/form-field/form-field.component';
import { InputErrorPipe } from '@shared/pipes/input-error.pipe';
import { BackendShellComponent } from '@layouts/backend-shell/backend-shell.component';
import { HomeShellComponent } from '@layouts/home-shell/home-shell.component';
import { NavBarComponent } from '@layouts/nav-bar/nav-bar.component';
import { HomeNavBarComponent } from '@layouts/home-nav-bar/home-nav-bar.component';

const MODULES = [
  ReactiveFormsModule,
  HttpClientModule,
  RouterModule,
  NgbModule,
  FormsModule,
];

const DECLARATIONS = [
  BackendShellComponent,
  HomeShellComponent,
  NavBarComponent,
  HomeNavBarComponent,
  InputErrorPipe,
  FormFieldComponent,

];

@NgModule({
  declarations: [
    ...DECLARATIONS,
  ],
  imports: [
    CommonModule,
    ...MODULES,
  ],
  exports: [
    ...MODULES,
    ...DECLARATIONS,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
