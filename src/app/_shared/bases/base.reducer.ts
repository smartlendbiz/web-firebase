export { createReducer, on, Action } from '@ngrx/store';

export interface BaseState {
    error?: any;
    isLoading?: boolean;
}

export const initialState: BaseState = {
    error: null,
    isLoading: false,
}
