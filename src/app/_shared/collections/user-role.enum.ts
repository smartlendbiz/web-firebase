export enum UserRole {
    SUPERUSER = 'SUPERUSER',
    MEMBER = 'MEMBER',
    ADMIN = 'ADMIN',
}
