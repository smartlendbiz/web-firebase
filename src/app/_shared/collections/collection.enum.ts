export enum Collection {
    GROUPS = 'Groups',
    USERS = 'Users',
    USER_INVITES = 'UserInvites',
}
