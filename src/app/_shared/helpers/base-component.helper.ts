import { FormControlName, FormGroup } from '@angular/forms';
import { OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';

export class BaseCompnent implements OnDestroy  {
    public subs = new SubSink();

    isValid(field: FormControlName) {
        return field.valid && (field.dirty || field.touched);
    }

    isNotValid(field: FormControlName) {
        return field.invalid && (field.dirty || field.touched);
    }

    cleanForm(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach((key) => formGroup.get(key).setValue(formGroup.get(key).value.trim()));
    }

    getErrorMessage(formControlName: FormControlName, form?: any) {
        const { errors } = formControlName;
        const { submitted } = form;

        // tslint:disable-next-line: forin
        for (const error in errors) {
            switch (error) {
                case 'required':
                    return submitted ? 'This field is required' : '';
                case 'email':
                    return submitted ? 'Invalid email format' : '';
                case 'minlength':
                    return submitted ? `Minimum length should be ${ errors[error].requiredLength }` : '';
                case 'maxlength':
                    return submitted ? `Miximum length should be ${ errors[error].requiredLength }` : '';
            }
        }

        return '';
    }

    ngOnDestroy(): void {
      this.subs.unsubscribe();
    }
}
