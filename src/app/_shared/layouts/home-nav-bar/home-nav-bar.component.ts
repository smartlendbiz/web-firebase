import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-nav-bar',
  templateUrl: './home-nav-bar.component.html',
  styleUrls: ['./home-nav-bar.component.scss']
})
export class HomeNavBarComponent implements OnInit {
  collapsed = true;
  items: any;

  constructor() { }

  ngOnInit() {
    this.items = [
      { title: 'Home', path: '/' },
      { title: 'How It Works', path: '/how-it-works' },
      { title: 'About', path: '/about' },
      { title: 'Contact', path: '/contact' },
    ];
  }
}
