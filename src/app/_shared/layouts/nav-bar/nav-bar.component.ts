import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';

import { AuthState } from '@modules/auth/store/reducers';
import { logout } from '@rootstore/index';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  collapsed = true;
  items: any;

  constructor(
    private store: Store<AuthState>
  ) {
  }

  ngOnInit() {
    this.items = [
      { title: 'Dashboard', path: '/dashboard' },
      { title: 'Wallet', path: '/wallet' },
      { title: 'Contribution', path: '/contribution' },
      { title: 'Loan', path: '/loan' },
    ];
  }

  onLogout() {
    this.store.dispatch(logout());
  }
}
