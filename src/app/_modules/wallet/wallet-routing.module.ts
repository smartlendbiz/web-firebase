import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WalletIndexComponent } from './wallet-index/wallet-index.component';
import { TransferComponent } from './transfer/transfer.component';
import { CashOutComponent } from './cash-out/cash-out.component';
import { CashInComponent } from './cash-in/cash-in.component';


const routes: Routes = [
  {
    path: '',
    component: WalletIndexComponent,
  },
  {
    path: 'cash-in',
    component: CashInComponent,
  },
  {
    path: 'cash-out',
    component: CashOutComponent,
  },
  {
    path: 'transfer',
    component: TransferComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WalletRoutingModule { }
