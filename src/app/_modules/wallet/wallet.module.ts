import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/modules/shared/shared.module';
import { WalletRoutingModule } from './wallet-routing.module';
import { CashInComponent } from './cash-in/cash-in.component';
import { CashOutComponent } from './cash-out/cash-out.component';
import { TransferComponent } from './transfer/transfer.component';
import { WalletIndexComponent } from './wallet-index/wallet-index.component';

@NgModule({
  declarations: [
    CashInComponent, CashOutComponent, TransferComponent, WalletIndexComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    WalletRoutingModule
  ]
})
export class WalletModule { }
