import { SharedModule } from '@shared/modules/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { IndexComponent } from './index/index.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { HowItWorksComponent } from './how-it-works/how-it-works.component';
import { HomeNavBarComponent } from '@shared/layouts/home-nav-bar/home-nav-bar.component';


@NgModule({
  declarations: [
    IndexComponent,
    AboutComponent,
    ContactComponent,
    HowItWorksComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
