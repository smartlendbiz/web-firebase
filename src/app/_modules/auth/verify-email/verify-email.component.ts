import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AuthState } from './../store/reducers';
import { verifyEmail } from './../store/actions';
import { verifyCode } from './../store/actions';
import { selectIsLoading } from './../store/selectors/verify-code.selector';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {
  loading$: Observable<boolean>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AuthState>
  ) {
    const mode = this.route.snapshot.queryParamMap.get('mode');
    const oobCode = this.route.snapshot.queryParamMap.get('oobCode');
    this.loading$ = store.select(selectIsLoading);
    store.dispatch(verifyCode({code: oobCode}));

    if (mode === 'resetPassword') {
      router.navigateByUrl(`/auth/change-password?oobCode=${oobCode}`);
    } else {
      store.dispatch(verifyEmail({ actionCode: oobCode }));
    }

  }

  ngOnInit() {
  }

}
