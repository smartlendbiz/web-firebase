import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register-sent',
  templateUrl: './register-sent.component.html',
  styleUrls: ['./register-sent.component.scss']
})
export class RegisterSentComponent implements OnInit {
  email: string;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.email = this.route.snapshot.queryParamMap.get('email');
  }
}
