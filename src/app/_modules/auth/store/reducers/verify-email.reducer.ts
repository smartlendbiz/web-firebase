import * as baseReducer from '@shared/bases/base.reducer';
import * as verifyEmailActions from '../actions';

// tslint:disable-next-line: no-empty-interface
export interface State extends baseReducer.BaseState { }

const verifyEmailReducer = baseReducer.createReducer(
    baseReducer.initialState,
    baseReducer.on(verifyEmailActions.verifyEmail, state => ({
        ...state, isLoading: true
    })),
    baseReducer.on(verifyEmailActions.verifyEmailSuccess, state => ({
        ...state, isLoading: false, error: null
    })),
    baseReducer.on(verifyEmailActions.verifyEmailFailed, (state, { error }) => ({
        ...state, error, isLoading: false
    }))
);

export function reducer(state: State | undefined, action: baseReducer.Action) {
    return verifyEmailReducer(state, action);
}
