import * as baseReducer from '@shared/bases/base.reducer';
import * as verifyCodeActions from '../actions';

// tslint:disable-next-line: no-empty-interface
export interface State extends baseReducer.BaseState {
  isCodeVerified: boolean;
}

const initialState: State = {
  ...baseReducer.initialState,
  isCodeVerified: false,
}

const verifyCodeReducer = baseReducer.createReducer(
    initialState,
    baseReducer.on(verifyCodeActions.verifyCode, state => ({
        ...state, isLoading: true
    })),
    baseReducer.on(verifyCodeActions.verifyCodeSuccess, state => ({
        ...state, isLoading: false, error: null, isCodeVerified: true,
    })),
    baseReducer.on(verifyCodeActions.verifyCodeFailed, (state, { error }) => ({
        ...state, error, isLoading: false, isCodeVerified: false,
    }))
);

export function reducer(state: State | undefined, action: baseReducer.Action) {
    return verifyCodeReducer(state, action);
}
