import * as baseReducer from '@shared/bases/base.reducer';
import * as loginActions from '../actions';

// tslint:disable-next-line: no-empty-interface
export interface State extends baseReducer.BaseState { }

const loginReducer = baseReducer.createReducer(
    baseReducer.initialState,
    baseReducer.on(loginActions.login, state => ({
        ...state, isLoading: true
    })),
    baseReducer.on(loginActions.loginSuccess, state => ({
        ...state, isLoading: false, error: null
    })),
    baseReducer.on(loginActions.loginFailed, (state, { error }) => ({
        ...state, error, isLoading: false
    }))
);

export function reducer(state: State | undefined, action: baseReducer.Action) {
    return loginReducer(state, action);
}
