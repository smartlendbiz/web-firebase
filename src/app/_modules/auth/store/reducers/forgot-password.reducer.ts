import * as baseReducer from '@shared/bases/base.reducer';
import * as forgotPasswordActions from '../actions';

// tslint:disable-next-line: no-empty-interface
export interface State extends baseReducer.BaseState {
  isEmailSent: boolean;
}

const initialState: State = {
  isEmailSent: false,
};

const forgotPasswordReducer = baseReducer.createReducer(
    initialState,
    baseReducer.on(forgotPasswordActions.forgotPassword, state => ({
        ...state, isLoading: true
    })),
    baseReducer.on(forgotPasswordActions.forgotPasswordSuccess, state => ({
        ...state, isLoading: false, error: null, isEmailSent: true,
    })),
    baseReducer.on(forgotPasswordActions.forgotPasswordFailed, (state, { error }) => ({
        ...state, error, isLoading: false, isEmailSent: false,
    }))
);

export function reducer(state: State | undefined, action: baseReducer.Action) {
    return forgotPasswordReducer(state, action);
}
