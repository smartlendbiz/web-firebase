import * as baseReducer from '@shared/bases/base.reducer';
import * as registerActions from '../actions';

// tslint:disable-next-line: no-empty-interface
export interface State extends baseReducer.BaseState {
  hostMemberName: string;
  existingGroupName: string;
  isInvited: boolean;
}

const initialState: State = {
  ...baseReducer.initialState,
  hostMemberName: null,
  existingGroupName: null,
  isInvited: false,
}

const registerReducer = baseReducer.createReducer(
    initialState,
    // Register
    baseReducer.on(registerActions.register, state => ({
        ...state, isLoading: true
    })),
    baseReducer.on(registerActions.registerSuccess, state => ({
        ...state, isLoading: false, error: null
    })),
    baseReducer.on(registerActions.registerFailed, (state, { error }) => ({
        ...state, error, isLoading: false
    })),

    // Create Group
    baseReducer.on(registerActions.createGroup, state => ({
        ...state, isLoading: true
    })),
    baseReducer.on(registerActions.createGroupSuccess, state => ({
        ...state, isLoading: false, error: null
    })),
    baseReducer.on(registerActions.createGroupFailed, (state, { error }) => ({
        ...state, error, isLoading: false
    })),

    // Create Group
    baseReducer.on(registerActions.createUser, state => ({
        ...state, isLoading: true
    })),
    baseReducer.on(registerActions.createUserSuccess, state => ({
        ...state, isLoading: false, error: null
    })),
    baseReducer.on(registerActions.createUserFailed, (state, { error }) => ({
        ...state, error, isLoading: false
    })),

    // Invite Code Verification
    baseReducer.on(registerActions.verifyInviteCode, state => ({
      ...state, isLoading: true
    })),
    baseReducer.on(registerActions.verifyInviteCodeSuccess, (state, {hostMemberName, existingGroupName, isInvited}) => ({
      ...state, isLoading: false, error: null, hostMemberName, existingGroupName, isInvited,
    })),
    baseReducer.on(registerActions.verifyInviteCodeFailed, (state, { error }) => ({
      ...state, error, isLoading: false
    })),
);

export function reducer(state: State | undefined, action: baseReducer.Action) {
    return registerReducer(state, action);
}
