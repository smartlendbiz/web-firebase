import * as baseReducer from '@shared/bases/base.reducer';
import * as changePasswordActions from '../actions';

// tslint:disable-next-line: no-empty-interface
export interface State extends baseReducer.BaseState {
  isPasswordChanged: boolean;
}

const initialState: State = {
  ...baseReducer.initialState,
  isPasswordChanged: false,
}

const changePasswordReducer = baseReducer.createReducer(
    initialState,
    baseReducer.on(changePasswordActions.changePassword, state => ({
        ...state, isLoading: true
    })),
    baseReducer.on(changePasswordActions.changePasswordSuccess, state => ({
        ...state, isLoading: false, error: null, isPasswordChanged: true,
    })),
    baseReducer.on(changePasswordActions.changePasswordFailed, (state, { error }) => ({
        ...state, error, isLoading: false, isPasswordChanged: false,
    }))
);

export function reducer(state: State | undefined, action: baseReducer.Action) {
    return changePasswordReducer(state, action);
}
