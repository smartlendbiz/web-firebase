import { ActionReducerMap } from '@ngrx/store';
import * as fromLogin from './login.reducer';
import * as fromRegister from './register.reducer';
import * as fromVerifyEmail from './verify-email.reducer';
import * as forgotPassword from './forgot-password.reducer';
import * as changePassword from './change-password.reducer';
import * as verifyCode from './verify-code.reducer';

export class AuthState {
    login: fromLogin.State;
    register: fromRegister.State;
    emailVerify: fromVerifyEmail.State;
    forgotPassword: forgotPassword.State;
    changePassword: changePassword.State;
    verifyCode: verifyCode.State;
}

export const reducers: ActionReducerMap<AuthState> = {
    login: fromLogin.reducer,
    register: fromRegister.reducer,
    emailVerify: fromVerifyEmail.reducer,
    forgotPassword: forgotPassword.reducer,
    changePassword: changePassword.reducer,
    verifyCode: verifyCode.reducer,
};
