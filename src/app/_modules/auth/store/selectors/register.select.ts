import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from '../reducers';

const selectAuthState = createFeatureSelector<AuthState>('auth');

export const selectRegisterState = createSelector(selectAuthState, state => state.register);
export const selectErrorMessage = createSelector(selectRegisterState, state => state.error);
export const selectIsLoading = createSelector(selectRegisterState, state => state.isLoading);
export const selectIsInvited = createSelector(selectRegisterState, state => state.isInvited);
export const selectHostMemberName = createSelector(selectRegisterState, state => state.hostMemberName);
export const selectExistingGroupName = createSelector(selectRegisterState, state => state.existingGroupName);
