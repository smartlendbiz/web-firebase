import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from '../reducers';

const selectAuthState = createFeatureSelector<AuthState>('auth');

export const selectLoginState = createSelector(
    selectAuthState,
    state => state.login
);

export const selectErrorMessage = createSelector(
    selectLoginState,
    state => state.error
);

export const selectIsLoading = createSelector(
    selectLoginState,
    state => state.isLoading
);
