import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from '../reducers';

const selectAuthState = createFeatureSelector<AuthState>('auth');

export const selectVerifyCodeState = createSelector(selectAuthState, state => state.verifyCode);
export const selectErrorMessage = createSelector(selectVerifyCodeState, state => state.error);
export const selectIsLoading = createSelector(selectVerifyCodeState, state => state.isLoading);
export const selectIsCodeVerified = createSelector(selectVerifyCodeState, state => state.isCodeVerified);
