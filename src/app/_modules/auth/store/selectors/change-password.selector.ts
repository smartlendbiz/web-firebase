import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from '../reducers';

const selectAuthState = createFeatureSelector<AuthState>('auth');

export const selectChangePasswordState = createSelector(selectAuthState, state => state.changePassword);
export const selectErrorMessage = createSelector(selectChangePasswordState, state => state.error);
export const selectIsLoading = createSelector(selectChangePasswordState, state => state.isLoading);
export const selectIsPasswordChanged = createSelector(selectChangePasswordState, state => state.isPasswordChanged);

