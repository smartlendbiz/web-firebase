import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from '../reducers';

const selectAuthState = createFeatureSelector<AuthState>('auth');

export const selectForgotPasswordState = createSelector(selectAuthState, state => state.forgotPassword);
export const selectErrorMessage = createSelector(selectForgotPasswordState, state => state.error);
export const selectIsLoading = createSelector(selectForgotPasswordState, state => state.isLoading);
export const selectIsEmailSent = createSelector(selectForgotPasswordState, state => state.isEmailSent);

