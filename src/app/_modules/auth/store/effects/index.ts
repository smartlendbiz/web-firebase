import { RegisterEffects } from './register.effect';
import { LoginEffects } from './login.effect';
import { VerifyEmailEffects } from './verify-email.effect';
import { ForgotPasswordEffects } from './forgot-password.effect';
import { VerifyCodeEffects } from './verify-code.effect';
import { ChangePasswordEffects } from './change-password.effect';

export const authEffects: any[] = [
    LoginEffects,
    RegisterEffects,
    VerifyEmailEffects,
    ForgotPasswordEffects,
    VerifyCodeEffects,
    ChangePasswordEffects,
];


// Exports
export * from './login.effect';
export * from './register.effect';
export * from './verify-email.effect';
export * from './forgot-password.effect';
export * from './verify-code.effect';
export * from './change-password.effect';
