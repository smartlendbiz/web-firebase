import { Router } from '@angular/router';
import { of, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, tap, exhaustMap, map } from 'rxjs/operators';

import { AuthService } from '@services/auth.service';
import * as verifyEmailActions from '../actions/verify-email.action';

@Injectable()
export class VerifyEmailEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private service: AuthService
    ) { }

    verifyEmail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(verifyEmailActions.verifyEmail),
            exhaustMap(({ actionCode }) =>
                from(this.service.verifyEmail(actionCode)).pipe(
                    map(() => verifyEmailActions.verifyEmailSuccess()),
                    catchError(error => of(verifyEmailActions.verifyEmailFailed({ error: error.message }))),
                )
            )
        )
    );

    verifyEmailFailed$ = createEffect(() =>
        this.actions$.pipe(
            ofType(verifyEmailActions.verifyEmailFailed),
            tap(() => this.router.navigate(['auth/expired']))
        ),
        { dispatch: false }
    );
}
