import { Router } from '@angular/router';
import { of, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, tap, concatMap, exhaustMap, map } from 'rxjs/operators';

import { AuthService } from '@services/auth.service';
import * as registerActions from '../actions/register.action';

@Injectable()
export class RegisterEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private service: AuthService
    ) { }

    register$ = createEffect(() =>
        this.actions$.pipe(
            ofType(registerActions.register),
            exhaustMap(({ newUser, groupName, inviteCode }) => {
                if (!!groupName) {
                    return from(this.service.createGroup(groupName)).pipe(
                        map(group => registerActions.createUser({ newUser, defaultGroup: { id: group.id, name: groupName }, inviteCode })),
                        catchError(error => of(registerActions.createGroupFailed({ error: error.message }))),
                    );
                }

                return of(registerActions.registerFailed({ error: 'Failed! Incomplete Entry.' }))
            })
        )
    );

    createUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(registerActions.createUser),
            exhaustMap(({ newUser, defaultGroup, inviteCode }) =>
                from(this.service.createUser(newUser, defaultGroup, inviteCode)).pipe(
                    concatMap(email => [
                        registerActions.createUserSuccess({ email }),
                        registerActions.registerSuccess()
                    ]),
                    catchError(error => of(registerActions.createUserFailed({ error: error.message }))),
                )
            )
        )
    );

    createUserSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(registerActions.createUserSuccess),
            tap(({ email }) => this.router.navigateByUrl(`/auth/register-sent?email=${ email }`)),
        ),
        { dispatch: false }
    );

    createUserFailed$ = createEffect(() =>
        this.actions$.pipe(
            ofType(registerActions.createUserFailed),
            exhaustMap(error => of(registerActions.registerFailed({ error })))
        ),
        { dispatch: false }
    );

    verifyInviteCode$ = createEffect(() =>
          this.actions$.pipe(
            ofType(registerActions.verifyInviteCode),
            exhaustMap(({ inviteCode }) =>
              from(this.service.getInviteDetails(inviteCode)).pipe(
                map(({hostMemberName, existingGroupName, isInvited}) =>
                  registerActions.verifyInviteCodeSuccess({hostMemberName, existingGroupName, isInvited})
                ),
                catchError(error => of(registerActions.verifyInviteCodeFailed({error: error.message})))
              )
            )
          )
    )
}
