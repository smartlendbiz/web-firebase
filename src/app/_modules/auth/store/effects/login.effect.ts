import { Router } from '@angular/router';
import { of, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, tap, concatMap, exhaustMap, map } from 'rxjs/operators';

import { AuthService } from '@services/auth.service';
import * as loginActions from '../actions/login.action';
import * as rootActions from '@rootstore/root.action';

@Injectable()
export class LoginEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private service: AuthService
    ) { }

    login$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loginActions.login),
            exhaustMap(({ email, password, rememberMe }) =>
                from(this.service.login({ email, password, rememberMe })).pipe(
                    concatMap(payload => {
                        if (payload.user.emailVerified) {
                            return [loginActions.loginSuccess(), rootActions.setAuthUser()];
                        }

                        return [loginActions.loginFailed({ error: 'Email is not yet verified. Check your inbox for the link.' })];
                    }),
                    catchError(error => of(loginActions.loginFailed({ error: 'Invalid Credentials' }))),
                )
            )
        )
    );

    loginSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loginActions.loginSuccess),
            tap(() => this.router.navigate(['/dashboard'])),
        ),
        { dispatch: false }
    );
}
