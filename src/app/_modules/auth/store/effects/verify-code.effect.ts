import { Router } from '@angular/router';
import { of, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, tap, exhaustMap, map } from 'rxjs/operators';

import { AuthService } from '@services/auth.service';
import * as verifyCodeActions from '../actions/verify-code.action';

@Injectable()
export class VerifyCodeEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private service: AuthService
    ) { }

    verifyCode$ = createEffect(() =>
        this.actions$.pipe(
            ofType(verifyCodeActions.verifyCode),
            exhaustMap(({ code }) =>
                from(this.service.verifyCode(code)).pipe(
                    map(() => verifyCodeActions.verifyCodeSuccess()),
                    catchError(error => of(verifyCodeActions.verifyCodeFailed({ error: error.message }))),
                )
            )
        )
    );

    verifyCodeFailed$ = createEffect(() =>
        this.actions$.pipe(
            ofType(verifyCodeActions.verifyCodeFailed),
            tap(() => this.router.navigate(['auth/expired']))
        ),
        { dispatch: false }
    );
}
