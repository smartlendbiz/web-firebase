import { Router } from '@angular/router';
import { of, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, tap, exhaustMap, map } from 'rxjs/operators';

import { AuthService } from '@services/auth.service';
import * as changePasswordActions from '../actions/change-password.action';

@Injectable()
export class ChangePasswordEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private service: AuthService
    ) { }

    changePassword$ = createEffect(() =>
        this.actions$.pipe(
            ofType(changePasswordActions.changePassword),
            exhaustMap(({ code, password }) =>
                from(this.service.changePassword(code, password)).pipe(
                    map(() => changePasswordActions.changePasswordSuccess()),
                    catchError(error => of(changePasswordActions.changePasswordFailed({ error: error.message }))),
                )
            )
        )
    );
}
