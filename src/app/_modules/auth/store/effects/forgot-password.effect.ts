import { Router } from '@angular/router';
import { of, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, tap, exhaustMap, map } from 'rxjs/operators';

import { AuthService } from '@services/auth.service';
import * as forgotPasswordActions from '../actions/forgot-password.action';

@Injectable()
export class ForgotPasswordEffects {
    constructor(
        private actions$: Actions,
        private router: Router,
        private service: AuthService
    ) { }

    forgotPassword$ = createEffect(() =>
        this.actions$.pipe(
            ofType(forgotPasswordActions.forgotPassword),
            exhaustMap(({ email }) =>
                from(this.service.resetPassword(email)).pipe(
                    map(() => forgotPasswordActions.forgotPasswordSuccess()),
                    catchError(error => of(forgotPasswordActions.forgotPasswordFailed({ error: error.message }))),
                )
            )
        )
    );
}
