import { createAction, props } from '@ngrx/store';

export const forgotPassword = createAction('[Forgot Password Page] Forgot Password', props<{ email: string }>());
export const forgotPasswordSuccess = createAction('[Forgot Password Page] Forgot Password Success');
export const forgotPasswordFailed = createAction('[Forgot Password Page] Forgot Password Failed', props<{ error: any }>());
