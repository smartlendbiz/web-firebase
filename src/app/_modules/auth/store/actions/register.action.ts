import { DefaultGroupModel } from '@models/users';
import { createAction, props } from '@ngrx/store';

import { UserModel } from '@models/users/user.model';

// Register
export const register = createAction('[Register Page] Registration',
  props<{ newUser: UserModel, groupName?: string, inviteCode?: string }>()
);
export const registerSuccess = createAction('[Register Page] Registration Success');
export const registerFailed = createAction('[Register Page] Registration Failed', props<{ error: any }>());

// Create Group
export const createGroup = createAction('[Register Page] Group Creation', props<{ groupName: string }>());
export const createGroupSuccess = createAction('[Register Page] Group Creation Success', props<{ uuid: string }>());
export const createGroupFailed = createAction('[Register Page] Group Creation Failed', props<{ error: any }>());

// Create User
export const createUser = createAction('[Register Page] User Creation',
    props<{ newUser: UserModel, defaultGroup: DefaultGroupModel, inviteCode: string }>()
);
export const createUserSuccess = createAction('[Register Page] User Creation Success', props<{ email: string }>());
export const createUserFailed = createAction('[Register Page] User Creation Failed', props<{ error: any }>());

// Verify Invite Code
export const verifyInviteCode = createAction('[Register Page] Invite Code Verification', props<{ inviteCode: string }>());
export const verifyInviteCodeSuccess = createAction('[Register Page] Invite Code Verification Success',
  props<{ hostMemberName?: string, existingGroupName: string, isInvited: boolean }>()
);
export const verifyInviteCodeFailed = createAction('[Register Page] Invite Code Verification Failed', props<{ error: any }>());

// Create User Invite Record
export const createUserInviteRecord = createAction('[Register Page] Create User Invite Record', props<{ inviteCode: string }>());
export const createUserInviteRecordSuccess = createAction('[Register Page] Create User Invite Record Success');
export const createUserInviteRecordFailed = createAction('[Register Page] Create User Invite Record Failed', props<{ error: any }>());

