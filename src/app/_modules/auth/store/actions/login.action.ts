import { createAction, props } from '@ngrx/store';

export const login = createAction('[Login Page] Login', props<{ email: string, password: string, rememberMe: boolean }>());
export const loginSuccess = createAction('[Login Page] Login Success');
export const loginFailed = createAction('[Login Page] Login Failed', props<{ error: any }>());
