import { createAction, props } from '@ngrx/store';

export const verifyEmail = createAction('[Email Verification Redirect] Verifying Email', props<{ actionCode: string }>());
export const verifyEmailSuccess = createAction('[Email Verification Redirect] Verifying Email Success');
export const verifyEmailFailed = createAction('[Email Verification Redirect] Verifying Email Failed', props<{ error: any }>());
