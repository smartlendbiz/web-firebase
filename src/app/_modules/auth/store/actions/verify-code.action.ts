import { createAction, props } from '@ngrx/store';

export const verifyCode = createAction('[Verify Code Action ] Verify Code Action', props<{ code: string }>());
export const verifyCodeSuccess = createAction('[Verify Code Action] Verify Code Action Success');
export const verifyCodeFailed = createAction('[Verify Code Action ] Verify Code Action Failed', props<{ error: any }>());
