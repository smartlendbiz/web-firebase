import { createAction, props } from '@ngrx/store';

export const changePassword = createAction('[Change Password Page] Change Password', props<{ code: string, password: string }>());
export const changePasswordSuccess = createAction('[Change Password Page] Change Password Success');
export const changePasswordFailed = createAction('[Change Password Page] Change Password Failed', props<{ error: any }>());
