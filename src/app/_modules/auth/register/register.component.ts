import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';


import { AuthState } from '@modules/auth/store/reducers';
import { BaseCompnent } from '@shared/helpers/base-component.helper';
import { register, verifyInviteCode } from './../store/actions';
import * as registerSelect from '../store/selectors/register.select';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends BaseCompnent implements OnInit {
  hostMemberName$: Observable<string>;
  existingGroupName$: Observable<string>;
  isLoading$: Observable<boolean>;
  errorMessage$: Observable<string>;

  registerForm: any;
  showPassword: boolean;
  isNewGroup: boolean;
  inviteCode: string;
  localError: string;
  isInvited: boolean;


  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private store: Store<AuthState>
  ) {
    super();
  }

  ngOnInit() {
    this.registerForm = this.fb.group({
      groupName: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      mobileNo: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(7)]],
    });
    this.inviteCode = this.route.snapshot.queryParamMap.get('inviteCode');

    if(!!this.inviteCode) {
      this.store.dispatch(verifyInviteCode({inviteCode: this.inviteCode}));
    }

    this.subs.sink = this.store.select(registerSelect.selectIsInvited).subscribe(isInvited => {
        this.isInvited = isInvited;
        if (isInvited) {
            this.registerForm.removeControl('groupName');
        }
    });

    this.hostMemberName$ = this.store.select(registerSelect.selectHostMemberName);
    this.existingGroupName$ = this.store.select(registerSelect.selectExistingGroupName);
    this.errorMessage$ = this.store.select(registerSelect.selectErrorMessage);
    this.isLoading$ = this.store.select(registerSelect.selectIsLoading);

  }

  get groupName() {
    return this.registerForm.get('groupName');
  }

  get firstname() {
    return this.registerForm.get('firstname');
  }

  get lastname() {
    return this.registerForm.get('lastname');
  }

  get mobileNo() {
    return this.registerForm.get('mobileNo');
  }

  get email() {
    return this.registerForm.get('email');
  }

  get password() {
    return this.registerForm.get('password');
  }

  onSubmit() {
    this.cleanForm(this.registerForm);

    if (this.isInvited && !!!this.isNewGroup) {
      this.localError = 'Creation of group is needed';
    }

    if (this.registerForm.dirty && this.registerForm.valid) {
      const { groupName, ...newUser } = this.registerForm.value;
      this.store.dispatch(register({ newUser, groupName, inviteCode: this.inviteCode }));
    }
  }
}
