import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { BaseCompnent } from '@shared/helpers/base-component.helper';
import { AuthState } from '@modules/auth/store/reducers';
import * as changePasswordSelect from '../store/selectors/change-password.selector';
import { changePassword } from '../store/actions';
import { verifyCode } from './../store/actions';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent extends BaseCompnent implements OnInit {
  showPassword: boolean;
  code: string;
  form: any;
  isPasswordChange$: Observable<boolean>;

  isLoading$: Observable<boolean>;
  errorMessage$: Observable<string>;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private store: Store<AuthState>
  ) {
    super();
  }

  ngOnInit() {
    this.code = this.route.snapshot.queryParamMap.get('oobCode');
    this.store.dispatch(verifyCode({ code: this.code }));

    this.form = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(7)]]
    });

    this.errorMessage$ = this.store.select(changePasswordSelect.selectErrorMessage);
    this.isLoading$ = this.store.select(changePasswordSelect.selectIsLoading);
    this.isPasswordChange$ = this.store.select(changePasswordSelect.selectIsPasswordChanged);
  }

  get password() {
    return this.form.get('password');
  }

  onSubmit() {
    if (this.form.valid) {
      this.store.dispatch(changePassword({ password: this.form.value.password, code: this.code }));
    }
  }

}
