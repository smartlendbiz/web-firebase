import { Observable } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';

import { BaseCompnent } from '@shared/helpers/base-component.helper';
import { AuthState } from '@modules/auth/store/reducers';
import * as forgotPasswordSelect from '../store/selectors/forgot-password.selector';
import { forgotPassword } from './../store/actions';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent extends BaseCompnent implements OnInit {
  form: any;
  emailSent: boolean;

  isLoading$: Observable<boolean>;
  errorMessage$: Observable<string>;

  constructor(
    private fb: FormBuilder,
    private store: Store<AuthState>
  ) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    });

    this.errorMessage$ = this.store.select(forgotPasswordSelect.selectErrorMessage);
    this.isLoading$ = this.store.select(forgotPasswordSelect.selectIsLoading);

    this.subs.sink = this.store.select(forgotPasswordSelect.selectIsEmailSent).subscribe(value => this.emailSent = value);
  }

  get email() {
    return this.form.get('email');
  }

  onSubmit() {
    this.cleanForm(this.form);
    if (this.form.valid) {
      this.store.dispatch(forgotPassword({ email: this.form.value.email }));
    }
  }
}
