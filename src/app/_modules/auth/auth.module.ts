import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@shared/modules/shared/shared.module';
import { CloseComponent } from '@shared/components/close/close.component';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { RegisterSentComponent } from './register-sent/register-sent.component';

import { authEffects } from './store/effects';
import { reducers } from './store/reducers';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { ExpiredComponent } from './expired/expired.component';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent,
    CloseComponent,
    RegisterSentComponent,
    VerifyEmailComponent,
    ExpiredComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    StoreModule.forFeature('auth', reducers),
    EffectsModule.forFeature(authEffects),
  ]
})
export class AuthModule { }
