import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';

import { BaseCompnent } from '@shared/helpers/base-component.helper';
import { AuthState } from '../store/reducers';
import { login } from '../store/actions';
import { selectErrorMessage, selectIsLoading } from '../store/selectors/login.select';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseCompnent implements OnInit {
  loginForm: any;
  showPassword: boolean;
  errorMessage$: Observable<string>;
  isLoading$: Observable<boolean>;

  constructor(
    private fb: FormBuilder,
    private store: Store<AuthState>
  ) {
    super();
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      rememberMe: [false],
    });

    this.errorMessage$ = this.store.select(selectErrorMessage);
    this.isLoading$ = this.store.select(selectIsLoading);
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  get rememberMe() {
    return this.loginForm.get('rememberMe');
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.store.dispatch(login({ ...this.loginForm.value }));
    }
  }
}
