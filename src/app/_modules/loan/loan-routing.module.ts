import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoanIndexComponent } from './loan-index/loan-index.component';

const routes: Routes = [
  {
    path: '',
    component: LoanIndexComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoanRoutingModule { }
