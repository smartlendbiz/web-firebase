import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanIndexComponent } from './loan-index.component';

describe('LoanIndexComponent', () => {
  let component: LoanIndexComponent;
  let fixture: ComponentFixture<LoanIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
