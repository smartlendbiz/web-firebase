import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/modules/shared/shared.module';
import { LoanRoutingModule } from './loan-routing.module';
import { LoanIndexComponent } from './loan-index/loan-index.component';


@NgModule({
  declarations: [LoanIndexComponent],
  imports: [
    CommonModule,
    SharedModule,
    LoanRoutingModule
  ]
})
export class LoanModule { }
