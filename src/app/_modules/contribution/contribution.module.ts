import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/modules/shared/shared.module';
import { ContributionRoutingModule } from './contribution-routing.module';
import { ContributionIndexComponent } from './contribution-index/contribution-index.component';


@NgModule({
  declarations: [ContributionIndexComponent],
  imports: [
    CommonModule,
    SharedModule,
    ContributionRoutingModule
  ]
})
export class ContributionModule { }
