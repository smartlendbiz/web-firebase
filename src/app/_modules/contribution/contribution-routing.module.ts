import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContributionIndexComponent } from './contribution-index/contribution-index.component';

const routes: Routes = [
  {
    path: '',
    component: ContributionIndexComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContributionRoutingModule { }
