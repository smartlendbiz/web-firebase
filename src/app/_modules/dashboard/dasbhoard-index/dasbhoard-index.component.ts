import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';

import { RootState, selectIsLogin } from '@rootstore/index';

@Component({
  selector: 'app-dasbhoard-index',
  templateUrl: './dasbhoard-index.component.html',
  styleUrls: ['./dasbhoard-index.component.scss']
})
export class DasbhoardIndexComponent implements OnInit {
  isLogin$: Observable<boolean>;

  constructor(
    private store: Store<RootState>
  ) { }

  ngOnInit() {
    this.isLogin$ = this.store.select(selectIsLogin);
  }

}
