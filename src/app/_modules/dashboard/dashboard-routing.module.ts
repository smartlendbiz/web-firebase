import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DasbhoardIndexComponent } from './dasbhoard-index/dasbhoard-index.component';


const routes: Routes = [
  {
    path: '',
    component: DasbhoardIndexComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
