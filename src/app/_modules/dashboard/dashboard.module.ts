import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/modules/shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DasbhoardIndexComponent } from './dasbhoard-index/dasbhoard-index.component';


@NgModule({
  declarations: [
    DasbhoardIndexComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
