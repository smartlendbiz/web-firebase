import { tap, map, flatMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { BehaviorSubject, Observable } from 'rxjs';

import { DefaultGroupModel, UserModel } from '@models/users';
import { UserRole } from '@collections/user-role.enum';
import { Collection } from '@collections/collection.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private eventAuthError = new BehaviorSubject<string>('');
  eventAuthError$ = this.eventAuthError.asObservable();
  private user: UserModel;

  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router,
  ) {
  }

  async createUser(user: UserModel, defaultGroup: DefaultGroupModel, inviteCode: string): Promise<any> {
    this.user = user;

    const userCredential = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
    await userCredential.user.updateProfile({
      displayName: `${ user.firstname } ${ user.lastname }`,
    });
    await this.insertNewUser(userCredential, defaultGroup, inviteCode);
    await this.afAuth.auth.currentUser.sendEmailVerification();
    await this.afAuth.auth.signOut();
    return user.email;
  }

  private async insertNewUser(userCredential: firebase.auth.UserCredential, defaultGroup: DefaultGroupModel, inviteCode: string) {
    const userRef = this.db.doc(`${ Collection.USERS }/${ userCredential.user.uid }`);
    const { id, password, ...newUser } = this.user;

    const InsertUser = {
      ...newUser,
      defaultGroup,
      groups: [defaultGroup.id],
      roles: !!inviteCode ? [UserRole.MEMBER] : [UserRole.SUPERUSER],
    };

    return await userRef.set(InsertUser);
  }


  login({ email, password, rememberMe }): Promise<any> {
    const auth = this.afAuth.auth;

    if (rememberMe) {
      auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
    }

    return auth.signInWithEmailAndPassword(email, password);
  }

  async resetPassword(email: string) {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  verifyEmail(actionCode: string): Promise<any> {
    return this.afAuth.auth.applyActionCode(actionCode);
  }

  verifyCode(code: string) {
    return this.afAuth.auth.verifyPasswordResetCode(code);
  }


  changePassword(code: string, newPassword: string) {
      return this.afAuth.auth.confirmPasswordReset(code, newPassword);
  }

  logout(): Promise<any> {
    return this.afAuth.auth.signOut();
  }

  setAuthUser(): Observable<any> {
    return this.afAuth.authState;
  }

  createGroup(groupName: string) {
    return this.db.collection(Collection.GROUPS).add({ name: groupName });
  }

  async getInviteDetails(inviteCode: string) {
    const notInvited = {
      isInvited: false,
      hostMemberName: '',
      existingGroupName: '',
    };

    try {
      const userInvite = await this.db.collection(Collection.USER_INVITES).doc(inviteCode).ref.get();
      const userInviteData = userInvite.data();
      if (userInviteData) {
        const { firstname, lastname, defaultGroup } = await this.getUserData(userInviteData.userId);
        return {
          hostMemberName: `${ firstname } ${ lastname }`,
          existingGroupName: defaultGroup.name,
          isInvited: true
        };
      }

      return notInvited;
    } catch (error) {
      return notInvited;
    }
  }

  async getUserData(userId: string) {
    const user = await this.db.collection(Collection.USERS).doc(userId).ref.get();
    return user.data();
  }

  async generateUserInvite() {
    // const userId = this.afAuth.auth.currentUser.uid;
    const userId = 'zvqET0oS9FUMq9U8lJjvmZan0iy1';
    const user = await this.db.collection(Collection.USERS).doc(userId).ref.get();
    const userData = user.data();
    if (userData) {
      const { defaultGroup } = userData;
      return await this.db.collection(Collection.USER_INVITES).add({ groupId: defaultGroup.id, userId, isUsed: false });
    }

  }

  get isLogin() {
    return this.afAuth.authState;
  }
}
