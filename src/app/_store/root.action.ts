import { createAction, props } from '@ngrx/store';
import { AuthUserModel } from '@models/users/auth-user.model';

export const showErrorMessage = createAction('Show Error Message');
export const hideErrorMessage = createAction('Hide Error Message');

export const showLoading = createAction('Show Loader');
export const hideLoading = createAction('Hide Loader');

export const setAuthUser = createAction('[Auth Action] Set Auth User');
export const setAuthUserSuccess = createAction('[Auth Action] Set Auth User Success', props<{ authUser: AuthUserModel }>());
export const setAuthUserFailed = createAction('[Auth Action] Set Auth User Failed', props<{ error: any }>());

export const unsetAuthUser = createAction('[Auth Action] Unset Auth User');

export const logout = createAction('[Auth Action] Logout');
export const logoutSuccess = createAction('[Auth Action] Logout Success');
export const logoutFailed = createAction('[Auth Action] Logout Failed');
