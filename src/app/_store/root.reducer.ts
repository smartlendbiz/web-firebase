import { createReducer, on, Action } from '@ngrx/store';
import * as rootActions from './root.action';
import { AuthUserModel } from '@models/users/auth-user.model';

export interface RootState {
    isError: boolean;
    isLoading: boolean;
    authUser: AuthUserModel | null;
}

export const intitialState: RootState = {
    isError: false,
    isLoading: false,
    authUser: null,
};


const rootReducer = createReducer(
    intitialState,
    on(rootActions.showErrorMessage, state => ({ ...state, isError: true })),
    on(rootActions.hideErrorMessage, state => ({ ...state, isError: false })),

    on(rootActions.showLoading, state => ({ ...state, isLoading: true })),
    on(rootActions.hideLoading, state => ({ ...state, isLoading: false })),

    on(rootActions.setAuthUser, state => state),
    on(rootActions.setAuthUserSuccess, (state, { authUser }) => ({ ...state, authUser })),
    on(rootActions.setAuthUserFailed, (state, { error }) => ({ ...state, authUser: null, error })),

    on(rootActions.unsetAuthUser, state => ({ ...state, authUser: null })),

    on(rootActions.logout, state => ({ ...state, isLoading: true })),
    on(rootActions.logoutSuccess, state => ({ ...state, isLoading: false, })),
    on(rootActions.logoutFailed, state => ({ ...state, isLoading: false, authUser: null })),
);

export function reducer(state: RootState | undefined, action: Action) {
    return rootReducer(state, action);
}
