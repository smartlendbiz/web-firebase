import { createSelector, createFeatureSelector } from '@ngrx/store';
import { RootState } from './root.reducer';


export const selectRootState = createFeatureSelector<RootState>('root');

export const selectRootIsLoading = createSelector(
    selectRootState,
    (state: RootState) => state.isLoading
);

export const selectRootIsError = createSelector(
    selectRootState,
    (state: RootState) => state.isError
);

export const selectAuthUser = createSelector(
    selectRootState,
    state => state.authUser
);

export const selectIsLogin = createSelector(
    selectAuthUser,
    authUser => !!authUser
);
