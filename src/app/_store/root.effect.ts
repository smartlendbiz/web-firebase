import { Router } from '@angular/router';
import { of, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, switchMap, map, tap, exhaustMap } from 'rxjs/operators';


import { AuthService } from '@services/auth.service';
import * as rootActions from './root.action';

@Injectable()
export class RootEffects {
    constructor(
        private actions$: Actions,
        private service: AuthService,
        private router: Router,
    ) { }

    setAuthUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(rootActions.setAuthUser),
            switchMap(action =>
                this.service.setAuthUser().pipe(
                    map(payload => rootActions.setAuthUserSuccess({ authUser: payload })),
                    catchError(error => of(rootActions.setAuthUserFailed({ error: error.message }))),
                )
            ),
        )
    );

    unSetAuthUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(rootActions.unsetAuthUser),
            exhaustMap(action =>
                from(this.service.logout()).pipe(
                    map(() => rootActions.unsetAuthUser())
                )
            ),
        )
    );

    logout$ = createEffect(() =>
        this.actions$.pipe(
            ofType(rootActions.logout),
            exhaustMap((action) =>
                from(this.service.logout()).pipe(
                    map((payload) => rootActions.logoutSuccess()),
                    catchError(error => of(rootActions.logoutFailed())),
                )
            )
        )
    );

    logoutSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(rootActions.logoutSuccess),
            tap(payload => {
                this.router.navigate(['/']);
            })
        ),
        { dispatch: false }
    );
}
