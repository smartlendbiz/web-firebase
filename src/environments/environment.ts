// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAFSHC4vO4IxOM8aPgOy3v5ROfxuaeM_SQ',
    authDomain: 'smartlend-b1837.firebaseapp.com',
    databaseURL: 'https://smartlend-b1837.firebaseio.com',
    projectId: 'smartlend-b1837',
    storageBucket: 'smartlend-b1837.appspot.com',
    messagingSenderId: '645869141664',
    appId: '1:645869141664:web:766955a1a8e86c4f5c93c5',
    measurementId: 'G-5SSN48RGH1'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
